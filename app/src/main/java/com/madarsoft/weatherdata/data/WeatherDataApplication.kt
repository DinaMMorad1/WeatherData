package com.madarsoft.weatherdata.data

import android.app.Application
import android.content.Context
import io.realm.Realm

class WeatherDataApplication: Application() {

    private var context: Context? = null

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        Realm.init(context)

    }

}