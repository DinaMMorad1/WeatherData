package com.madarsoft.weatherdata.viewmodel

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.madarsoft.weatherdata.BR
import com.madarsoft.weatherdata.api.RestHelper
import com.madarsoft.weatherdata.api.RestResponse
import com.madarsoft.weatherdata.managers.WeatherLocationManager
import com.madarsoft.weatherdata.model.WeatherLoc
import kotlin.properties.Delegates

class AddLocationViewModel(var context: Context) : BaseObservable() {

    var coordinates: LatLng? = null

    companion object {
        val NUM_SELECTED_LOCATIONS_TITLE = "Number Of Selected Locations : "
    }

    var locationsnum: Int by Delegates.observable(0) { prop, old, new ->
        locationsnumText = "$NUM_SELECTED_LOCATIONS_TITLE $locationsnum"
    }

    @get:Bindable
    var locationsnumText: String by Delegates.observable("$NUM_SELECTED_LOCATIONS_TITLE $locationsnum") { prop, old, new ->
        notifyPropertyChanged(BR.locationsnumText)
    }


    val lat: Double
        @Bindable get() {
            return if (coordinates != null) coordinates!!.latitude else 0.0
        }

    val lng: Double
        @Bindable get() {
            return if (coordinates != null) coordinates!!.longitude else 0.0
        }


    fun onLocationSelected(lat: Double, lng: Double) {
        locationsnum += 1
        RestHelper.getInstance(context).getWeatherData(lng, lat, object : RestResponse {
            override fun onSuccess(`object`: Any?) {
                var weatherLoc = `object` as WeatherLoc
                WeatherLocationManager.getInstance(context)?.addLocation(weatherLoc)
            }

            override fun onFailure(error: String?) {
                Log.i("", "" + `error`) // ToDo: if it was real app this should have displayed error message and marker can't be added without successful response
            }

            override fun onFailure(error: String?, errorCode: Int) {
                Log.i("", "" + error)
            }

        })
    }
}