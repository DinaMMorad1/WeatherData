package com.madarsoft.weatherdata.async.listeners;

public interface ResolveAddressListener {

    void onAddressReturned(String addressStr);

}
