package com.madarsoft.weatherdata.async;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.madarsoft.weatherdata.async.listeners.ResolveAddressListener;

import java.util.List;
import java.util.Locale;

public class ResolveAddressAsync extends AsyncTask {

    private Context context;
    private double longitude;
    private double latitude;
    private ResolveAddressListener listener;
    private String addressStr;

    public ResolveAddressAsync(Context context, double longitude, double latitude, ResolveAddressListener listener) {
        this.context = context;
        this.longitude = longitude;
        this.latitude = latitude;
        this.listener = listener;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        addressStr = getCompleteAddressString(latitude, longitude);
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (listener != null) {
            listener.onAddressReturned(addressStr);
        }
    }

    private String getCompleteAddressString(double latitude, double longitude) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }
}
