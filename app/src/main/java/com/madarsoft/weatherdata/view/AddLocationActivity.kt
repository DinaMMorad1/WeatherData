package com.madarsoft.weatherdata.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.madarsoft.weatherdata.R
import com.madarsoft.weatherdata.databinding.ActivityAddLocationBinding
import com.madarsoft.weatherdata.viewmodel.AddLocationViewModel
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.location.LocationServices
import com.google.android.gms.common.api.GoogleApiClient
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import android.content.Intent
import android.os.Build
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout


class AddLocationActivity : AppCompatActivity(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapLongClickListener {

    private var binding: ActivityAddLocationBinding? = null
    private var addLocationViewModel: AddLocationViewModel? = null
    private var googleApiClient: GoogleApiClient? = null
    private var map: GoogleMap? = null
    private var isLocationAdded = false

    protected val ACCESS_FINE_LOCATION_REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_location)

        initUI()
    }

    companion object {
        public val IS_LOCATION_ADDED = "IS_LOCATION_CHANGED"
    }

    private fun initUI() {
        initToolbar()
        addLocationViewModel = AddLocationViewModel(this@AddLocationActivity)
        binding?.viewmodel = addLocationViewModel

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        moveCurrentLocationButton()

        googleApiClient = GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build()
    }

    private fun initToolbar() {
        setSupportActionBar(binding?.toolbar)
        supportActionBar?.title = getString(R.string.title_activity_add_location)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
    }

    override fun onStart() {
        super.onStart()
        googleApiClient?.connect();
    }

    override fun onStop() {
        googleApiClient?.disconnect()
        super.onStop()
    }

    override fun onMapReady(p0: GoogleMap?) {
        map = p0
        map?.setOnMapLongClickListener(this@AddLocationActivity)
    }

    @SuppressLint("MissingPermission")
    private fun moveToCurrentLocation() {
        map?.isMyLocationEnabled = true
        LocationServices.getFusedLocationProviderClient(this).lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                val latLng = LatLng(location.latitude, location.longitude)
                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17f)
                map?.animateCamera(cameraUpdate)
            }
        }
    }

    override fun onConnected(p0: Bundle?) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            moveToCurrentLocation()
        } else {
            if (Build.VERSION.SDK_INT < 23) {
                moveToCurrentLocation()
            } else {
                requestPermissions(
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        ACCESS_FINE_LOCATION_REQUEST_CODE)
            }
        }
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onMapLongClick(point: LatLng?) {
        if (point != null) {
            map?.addMarker(MarkerOptions().
                    position(point).
                    icon(BitmapDescriptorFactory.
                            defaultMarker(BitmapDescriptorFactory.HUE_RED)))

            addLocationViewModel?.onLocationSelected(point.latitude, point.longitude)
            isLocationAdded = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ACCESS_FINE_LOCATION_REQUEST_CODE && grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                moveToCurrentLocation()
            }
        }
    }

    override fun onBackPressed() {
        val data = Intent()
        data.putExtra(IS_LOCATION_ADDED, isLocationAdded)
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    private fun moveCurrentLocationButton() {
        try {
            val locationButton = ((supportFragmentManager
                    .findFragmentById(R.id.map) as SupportMapFragment).view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(Integer.parseInt("2"))
            val rlp = locationButton.layoutParams as (RelativeLayout.LayoutParams)
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            rlp.setMargins(0, 0, 30, 30)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
