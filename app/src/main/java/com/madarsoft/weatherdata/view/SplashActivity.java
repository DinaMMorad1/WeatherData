package com.madarsoft.weatherdata.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.madarsoft.weatherdata.R;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1500;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        navigateLocationsWeather();
    }

    private void navigateLocationsWeather() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent signupIntent = new Intent(SplashActivity.this, LocationsWeatherDataActivity.class);
                startActivity(signupIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
