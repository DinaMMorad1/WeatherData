package com.madarsoft.weatherdata.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.madarsoft.weatherdata.R;
import com.madarsoft.weatherdata.adapters.LocationsWeatherAdapter;
import com.madarsoft.weatherdata.databinding.ActivityLocationsWeatherDataBinding;
import com.madarsoft.weatherdata.managers.WeatherLocationManager;
import com.madarsoft.weatherdata.model.WeatherLoc;

import java.util.List;

public class LocationsWeatherDataActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityLocationsWeatherDataBinding binding;
    private LocationsWeatherAdapter adapter;

    public final static int ADD_LOCATION_REQUEST_CODE = 123;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_locations_weather_data);
        List<WeatherLoc> weatherLocList = WeatherLocationManager.Companion.getInstance(this).getAll();
        setNoItemsVisibility(weatherLocList);

        adapter = new LocationsWeatherAdapter(weatherLocList, LocationsWeatherDataActivity.this);
        binding.recyclerviewLocations.setAdapter(adapter);
        binding.fabAddLocation.setOnClickListener(this);
        initToolbar();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_location:
                openAddLocationActivity();
                break;
        }
    }

    private void openAddLocationActivity() {
        Intent addLocationIntent = new Intent(this, AddLocationActivity.class);
        startActivityForResult(addLocationIntent, ADD_LOCATION_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_LOCATION_REQUEST_CODE) {
            if (data != null && data.getBooleanExtra(AddLocationActivity.Companion.getIS_LOCATION_ADDED(), false)) {
                refreshRecycler();
            }
        }
    }

    private void refreshRecycler() {
        List<WeatherLoc> weatherLocList = WeatherLocationManager.Companion.getInstance(this).getAll();
        setNoItemsVisibility(weatherLocList);
        if (adapter != null) {
            adapter.reloadData(weatherLocList);
        }
    }

    private void setNoItemsVisibility(List<WeatherLoc> weatherLocList) {
        if (weatherLocList == null || weatherLocList.size() == 0) {
            binding.txtviewNoitems.setVisibility(View.VISIBLE);
        } else {
            binding.txtviewNoitems.setVisibility(View.GONE);
        }
    }

    private void initToolbar() {
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_activity_locations_weather));
    }
}
