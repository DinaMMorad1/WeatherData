package com.madarsoft.weatherdata.api;

import android.content.Context;

import com.madarsoft.weatherdata.R;
import com.madarsoft.weatherdata.model.WeatherLoc;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestHelper {

    private static RestHelper instance;
    private Context context;
    private RestService restService;


    public static RestHelper getInstance(Context context) {
        if (instance == null) {
            instance = new RestHelper(context);
        }
        return instance;
    }

    private RestHelper(Context context) {
        this.context = context;
        getRestService();
    }

    public Retrofit getClient() {
        Retrofit retrofit = getRetrofitBuilder().build();
        return retrofit;
    }

    private Retrofit.Builder getRetrofitBuilder() {
        String baseUrl = "https://api.openweathermap.org/";
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create());
    }

    private void getRestService() {
        if (restService == null)
            restService = getClient().create(RestService.class);
    }

    public void getWeatherData(double longitude, double latitude, RestResponse restResponse) {
        Call<WeatherLoc> call = restService.getWeatherData(latitude, longitude, RestConstants.APP_ID);
        executeCall(call, restResponse);
    }

    public void getWeatherDataByCityId(int cityId, RestResponse restResponse) {
        Call<WeatherLoc> call = restService.getWeatherDataByCityId(cityId, RestConstants.APP_ID);
        executeCall(call, restResponse);
    }


    private void renderSuccess(RestResponse restResponse, Response response) {
        if (restResponse != null)
            restResponse.onSuccess(response.body());
    }

    private void renderError(RestResponse restResponse, Throwable throwable) {
        String failureMessage = "";
        if (restResponse != null && throwable != null && throwable.getLocalizedMessage() != null)
            failureMessage = throwable.getLocalizedMessage();
        restResponse.onFailure(failureMessage);
    }

    public void renderError(RestResponse restResponse, String errorMessage) {
        String failureMessage = "";
        if (restResponse != null && errorMessage != null)
            failureMessage = errorMessage;
        restResponse.onFailure(failureMessage);
    }

    public void renderError(RestResponse restResponse, String errorMessage, int errorCode) {
        String failureMessage = "";
        if (restResponse != null && errorMessage != null)
            failureMessage = errorMessage;
        restResponse.onFailure(failureMessage, errorCode);
    }

    private void executeCall(Call call, final RestResponse restResponse) {
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response != null && response.isSuccessful()) {
                    renderSuccess(restResponse, response);
                } else {
                    String errorMessage = context.getString(R.string.something_wrong);
                    if (response != null) {
                        APIError error = ErrorUtils.parseError(response, context);
                        errorMessage = error.message();
                    }
                    renderError(restResponse, errorMessage, response.code());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                renderError(restResponse, t);
            }
        });
    }


}
