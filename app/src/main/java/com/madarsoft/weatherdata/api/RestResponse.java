package com.madarsoft.weatherdata.api;

public interface RestResponse {

    void onSuccess(Object object);

    void onFailure(String error);

    void onFailure(String error, int errorCode);

}
