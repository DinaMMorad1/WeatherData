package com.madarsoft.weatherdata.api;

import com.madarsoft.weatherdata.model.WeatherLoc;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestService {

    @GET(RestConstants.GET_DAILY_FORECAST)
    Call<WeatherLoc> getWeatherData(@Query("lat") double lattitude, @Query("lon") double longitude, @Query("appid") String appId);

    @GET(RestConstants.GET_DAILY_FORECAST)
    Call<WeatherLoc> getWeatherDataByCityId(@Query("id") int cityId, @Query("appid") String appId);
}
