package com.madarsoft.weatherdata.managers

import android.content.Context
import com.madarsoft.weatherdata.model.Main
import com.madarsoft.weatherdata.model.WeatherItem
import com.madarsoft.weatherdata.model.WeatherLoc
import io.realm.Realm

class WeatherLocationManager private constructor(context: Context){

    companion object {
        private var instance: WeatherLocationManager? = null

        fun getInstance(context: Context): WeatherLocationManager? {
            if (instance == null)
                instance = WeatherLocationManager(context)

            return instance
        }
    }

    fun addLocation(location: WeatherLoc) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.insertOrUpdate(location.main)
        realm.insertOrUpdate(location.wind)
        for (item: WeatherItem in location.weatherItems) {
            realm.insertOrUpdate(item)
        }
        realm.insertOrUpdate(location)
        realm.commitTransaction()
    }

    fun getAll() : ArrayList<WeatherLoc> {
        val realm = Realm.getDefaultInstance()
        val realmResults = realm.where(WeatherLoc::class.java).findAll()
        val itemsArrayList = ArrayList<WeatherLoc>()
        itemsArrayList.addAll(realm.copyFromRealm(realmResults));
        return itemsArrayList
    }

}