package com.madarsoft.weatherdata.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class WeatherLoc extends RealmObject {

    @SerializedName("coord")
    private Coord coord;

    @SerializedName("name")
    private String name;

    @SerializedName("cood")
    private int cood;

    @SerializedName("id")
    @PrimaryKey
    private int id;

    @SerializedName("dt")
    private double dt;

    private String addressDetails;

    @SerializedName("main")
    private Main main;

    @SerializedName("wind")
    private Wind wind;

    @SerializedName("weather")
    private RealmList<WeatherItem> weatherItems;

    //
    // setters
    //

    public void setDt(double dt) {
        this.dt = dt;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCood(int cood) {
        this.cood = cood;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public void setWeatherItems(RealmList<WeatherItem> weatherItems) {
        this.weatherItems = weatherItems;
    }

    public void setAddressDetails(String addressDetails) {
        this.addressDetails = addressDetails;
    }

    //
    // getters
    //

    public double getDt() {
        return dt;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Coord getCoord() {
        return coord;
    }

    public int getCood() {
        return cood;
    }

    public Main getMain() {
        return main;
    }

    public Wind getWind() {
        return wind;
    }

    public RealmList<WeatherItem> getWeatherItems() {
        return weatherItems;
    }

    public String getAddressDetails() {
        return addressDetails;
    }
}
