package com.madarsoft.weatherdata.model;

import io.realm.RealmObject;

public class Coord extends RealmObject {

    private double lon;

    private double lat;

    //
    // setters
    //

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    //
    // getters
    //

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
