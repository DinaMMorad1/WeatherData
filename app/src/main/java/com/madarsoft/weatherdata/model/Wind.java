package com.madarsoft.weatherdata.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Wind extends RealmObject {

    @SerializedName("speed")
    private double speed;

    @SerializedName("deg")
    private double deg;

    //
    // setters
    //

    public void setDeg(double deg) {
        this.deg = deg;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    //
    // getters
    //

    public double getSpeed() {
        return speed;
    }

    public double getDeg() {
        return deg;
    }
}
