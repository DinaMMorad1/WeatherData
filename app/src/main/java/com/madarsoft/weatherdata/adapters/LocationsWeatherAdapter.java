package com.madarsoft.weatherdata.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.madarsoft.weatherdata.R;
import com.madarsoft.weatherdata.api.RestHelper;
import com.madarsoft.weatherdata.api.RestResponse;
import com.madarsoft.weatherdata.databinding.CellWeatherLocationBinding;
import com.madarsoft.weatherdata.model.WeatherItem;
import com.madarsoft.weatherdata.model.WeatherLoc;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LocationsWeatherAdapter extends RecyclerView.Adapter<LocationsWeatherAdapter.ViewHolder> {


    private List<WeatherLoc> weatherLocList;
    private Context context;

    public LocationsWeatherAdapter(List<WeatherLoc> weatherLocList, Context context) {
        this.weatherLocList = weatherLocList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        CellWeatherLocationBinding binding = DataBindingUtil.inflate(inflater, R.layout.cell_weather_location, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        WeatherLoc weatherLoc = weatherLocList.get(position);
        loadData(holder, weatherLoc);
        RestHelper.getInstance(context).getWeatherDataByCityId(weatherLoc.getId(), new RestResponse() {
            @Override
            public void onSuccess(Object object) {
                WeatherLoc updatedObj = (WeatherLoc) object;
                loadData(holder, updatedObj);
            }

            @Override
            public void onFailure(String error) {

            }

            @Override
            public void onFailure(String error, int errorCode) {

            }
        });
    }

    private void loadData(ViewHolder holder, WeatherLoc weatherLoc) {
        holder.binding.txtviewCityname.setText(weatherLoc.getName());

        if (weatherLoc.getWeatherItems() != null && weatherLoc.getWeatherItems().size() > 0) {
            WeatherItem weatherItem = weatherLoc.getWeatherItems().get(0);
            if (weatherItem.getIcon() != null && !weatherItem.getIcon().isEmpty()) {
                String imgUrl = "http://openweathermap.org/img/w/" + weatherItem.getIcon() + ".png";
                Picasso.get().load(imgUrl).into(holder.binding.imgviewWeathericon);
            }
            holder.binding.txtviewWeatherdescription.setText(weatherItem.getDescription());
        }
        if (weatherLoc.getMain() != null) {
            holder.binding.txtviewHumidity.setText(String.valueOf(weatherLoc.getMain().getHumidity()));
            holder.binding.txtviewPressure.setText(String.valueOf(weatherLoc.getMain().getPressure()));
            holder.binding.txtviewMinTemp.setText(String.valueOf(weatherLoc.getMain().getMinTemp()));
            holder.binding.txtviewMaxTemp.setText(String.valueOf(weatherLoc.getMain().getMaxTemp()));
        }
    }

    @Override
    public int getItemCount() {
        return weatherLocList.size();
    }

    public void reloadData(List<WeatherLoc> weatherLocList) {
        this.weatherLocList = weatherLocList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CellWeatherLocationBinding binding;

        public ViewHolder(CellWeatherLocationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

}
